<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class BaseEnumStatus extends Enum
{
    const PUBLISH = 0;
    const DRAFT = 1;
    const PENDING = 2;
}
