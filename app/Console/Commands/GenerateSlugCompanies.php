<?php

namespace App\Console\Commands;

use App\Models\Company;
use Illuminate\Console\Command;

class GenerateSlugCompanies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate slug for company';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (Company::all() as $company){
            $this->warn("Generate slug for : " . $company->name);
            $company->generateSlug();
            $company->save();
            $this->warn("After generated : " . $company->slug);
        }
        $this->info("Success Generate Slug");
        return self::SUCCESS;
    }
}
