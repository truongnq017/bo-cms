<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DeleteChaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:char';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (\File::exists(database_path('files/companies1_2000.sql'))) {
            $content = \File::get(database_path('files/companies1_2000.sql'));
            $content = str_replace("<<<ap>>>", " ", $content);
            \File::put(database_path('files/companies1_2000.sql'), $content);
        }
    }
}
