<?php

namespace App\Models;

use Bo\Base\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCompanyFavorite extends Model
{
    use HasFactory;
    use CrudTrait;

    protected $table = 'user_company_favorite';

    protected $fillable = [
        'user_id',
        'company_id',
    ];

    public function companies()
    {
        return $this->hasOne(Company::class,'id', 'company_id');
    }
}
