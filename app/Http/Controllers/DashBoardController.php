<?php

namespace App\Http\Controllers;

use App\Models\UserCompanyFavorite;
use Exception;
use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    public function user(Request $request)
    {
        $user = bo_auth()->user();

        return view('dashboard.user', compact('user'));
    }

    public function userFavorite(Request $request)
    {
        $user_favorites = UserCompanyFavorite::where('user_id', bo_user()->getKey())
            ->whereHas('companies', function ($query) {
                return $query->where('status', 'active');
            })
            ->with('companies')
            ->paginate(10);

        return view('dashboard.favorite', compact('user_favorites'));
    }

    public function addFavorite($company_id, Request $request)
    {

    }

    public function changeFavorite($company_id, Request $request)
    {
        try {
            $user_liked = UserCompanyFavorite::where('user_id', bo_user()->getKey())
                ->where('company_id', $company_id);

            $check = $user_liked->exists();

            $user_liked->delete();
            if ($check) {
                //nếu có thì là hành động xóa
                return json_encode([
                    'action' => 'remove'
                ]);
            } else {
                //không có thì thêm vào
                UserCompanyFavorite::create([
                    'user_id'    => bo_user()->getKey(),
                    'company_id' => $company_id
                ]);
                return json_encode([
                    'action' => 'add'
                ]);
            }
        } catch (Exception $exception) {
            return json_encode([
                'action' => 'fail',
                'message' => $exception->getMessage()
            ]);
        }
    }
}
