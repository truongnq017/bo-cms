<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index(Request $request)
    {
        $companies = Company::where('status', 'active')->orderBy('created_at', 'DESC')->paginate(10);
        return view('index', compact('companies'));
    }
}
