<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $companies = null;
        $search_key = null;

        if ($request->has('q') && !empty($request->input('q'))) {
            $companies = Company::where('status', 'active')
                ->where('name', 'like', '%' . $request->input('q') . '%')
                ->orderBy('created_at', 'DESC')
                ->paginate(10)
                ->appends(request()->query());;
            $search_key = $request->input('q');
        }

        return view('search.index', compact('companies', 'search_key'));
    }
}
