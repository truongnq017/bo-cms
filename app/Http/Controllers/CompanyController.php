<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function detail(Company $company)
    {
        return view('company.detail', compact('company'));
    }

    public function compareCompany($company_id, Request $request)
    {
        $search_key = null;
        $company_compare = null;
        $companies_compare = null;

        if ($request->has('compare-with')) {
            $search_key = $request->input('compare-with');
            $companies_compare = Company::where('status', 'active')
                ->where('name', 'like', '%' . $search_key . '%')
                ->where('id', '!=', $company_id)
                ->orderBy('created_at', 'DESC')
                ->paginate(10)
                ->appends(request()->query());;
        }

        if ($request->has('compare-to')) {
            $company_compare = Company::find($request->input('compare-to'));
        }


            $company = Company::findOrFail($company_id);
        return view('company.compare', compact(
            'company',
            'search_key',
            'company_compare',
            'companies_compare'
        ));
    }
}
