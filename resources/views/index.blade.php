@extends('main')

@section('title', __('Homepage'))

@push('css')
@endpush

@section('main')
    <div class="container mt-4">
        <div class="mt-4 mb-4">
            <a href="{{route('search')}}" class="btn btn-warning bg-warning-v2 border-0 text-white rounded">求人検索</a>
            @if(bo_auth()->guest())
                <a href="{{route('register.index')}}"
                   class="ml-5 btn btn-warning bg-warning-v2 border-0 text-white rounded">新規会員登録</a>
            @endif
        </div>
        @foreach ($companies as $company)
            <a href="{{route('company.detail',$company->slug ?? "")}}" class="text-decoration-none" style="color: inherit">
                <div class="d-flex mb-3" style="background-color: #f5f5f5">
                    <div class="border-home-info-company p-2" style="flex: 0 0 20%">
                        <p>スタンバイ</p>
                        <p><b style="word-break: break-word">{{$company->recruit_url}}</b></p>
                    </div>
                    <div class="border border-home-info-company p-2" style="flex: 0 0 25%">
                        <p class="text-center mb-0 text-white"
                           style="border: 1px solid black;background-color: rgb(59,126,163)">
                            有効掲載求人数</p>
                        <p class="text-center" style="border: 1px solid black">{{$company->salary}}</p>
                    </div>
                    <div class="border border-home-info-company p-2" style="flex-grow: 1">
                        <p><b>Indeed</b> {{$company->occupation}}
                        </p>
                    </div>
                </div>
            </a>
        @endforeach
        <div class="d-flex justify-content-center align-items-center">{{$companies->links()}}</div>
    </div>
@endsection

@push('javascript')
@endpush
