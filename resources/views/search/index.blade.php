@extends('main')

@section('title', __('Search'))

@push('css')
    <style>
        ul.pagination li:last-child a.page-link {
            background-color: #fff !important;
        }

        ul.pagination li:first-child a.page-link {
            background-color: #fff !important;
        }
    </style>
@endpush

@section('main')
    <div class="container mt-4">
        <div>
            <div class="text-center">
                <h4>検索</h4>
            </div>
            <form action="{{route('search')}}" method="GET">
                <div class="input-group">
                    <input type="text" name="q" value="{{$search_key}}" class="form-control" placeholder="検索">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button">
                            <i class="las la-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            <div class="list-group list-group-light pt-2">
                <a href="#" class="list-group-item list-group-item-action px-3 border-0 active"
                   aria-current="true">地域</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">業界</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">職種</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">制度や特徴</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">使用状況</a>
            </div>
        </div>
        @if($companies && $companies->count() > 0)
            <div class="text-center mt-2 mb-3">
                <h3>キーワードの検索結果 : {{$search_key}}</h3>
            </div>
            @foreach ($companies as $company)
                <div class="card-deck mb-3 text-center">
                    <div class="card mb-4 box-shadow">
                        <div class="card-header">
                            <h5 class="my-0 font-weight-normal">
                                {{$company->name}} <a href="{{$company->recruit_url}}">{{$company->recruit_url}}</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="d-flex justify-content-around">
                                <div class="w-50">
                                    <img class="w-100 border border-primary" src="{{$company->image}}"
                                         alt="{{$company->name}}">
                                </div>
                                <div>
                                    <h5 class="card-title pricing-card-title">
                                        有効掲載求人数
                                    </h5>
                                    <p>{{$company->salary}}</p>
                                    <div class="mt-3 mb-4">
                                        <strong>Indeed</strong><span>（{{$company->occupation}}</span>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('company.detail',$company->slug ?? "")}}"
                               class="btn btn-lg btn-block btn-outline-primary w-25 mt-4 ml-auto mr-auto font-sm">
                                <i class="las la-eye"></i> 詳細を見る
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="d-flex justify-content-center align-items-center">{{$companies->links()}}</div>
        @else
            @if($search_key)
                <div class="p-5 text-primary font-2xl text-center"><i class="las la-exclamation-circle"></i>
                    検索結果がありません。もう一度お試しください
                </div>
            @endif
        @endif
    </div>
@endsection

@push('javascript')
@endpush
