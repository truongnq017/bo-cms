@extends('main')

@section('title', __('Login'))

@section('main')
    <div class="row justify-content-center mt-4">
        <div class="col-12 col-md-8 col-lg-4">
            <h3 class="text-center mb-4">{{ trans('bo::base.login') }}</h3>
            <div class="card">
                <div class="card-body">
                    <form class="col-md-12 p-t-10" role="form" method="POST" action="{{ route('login.post') }}">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="control-label"
                                   for="{{ $username }}">{{ config('bo.base.authentication_column_name') }}</label>

                            <div>
                                <input type="text"
                                       class="form-control{{ $errors->has($username) ? ' is-invalid' : '' }}"
                                       name="{{ $username }}" value="{{ old($username) }}" id="{{ $username }}">

                                @if ($errors->has($username))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first($username) }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password">{{ trans('bo::base.password') }}</label>

                            <div>
                                <input type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       name="password" id="password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('bo::base.remember_me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div>
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ trans('bo::base.login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-center"><a href="{{ route('register.index') }}">{{ trans('bo::base.register') }}</a></div>
        </div>
    </div>
@endsection
