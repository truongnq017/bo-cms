@extends('main')

@section('title', __('Compare'))

@push('css')
    <style>
        ul.pagination li:last-child a.page-link {
            background-color: #fff !important;
        }

        ul.pagination li:first-child a.page-link {
            background-color: #fff !important;
        }
    </style>
@endpush

@section('main')
    <div class="container mt-4">
        <div>
            <div class="text-center">
                <h4>比較する検索 {{$company->name}}</h4>
            </div>
            <form action="{{route('company.compare', $company->id)}}" method="GET">
                <div class="input-group">
                    <input type="text" name="compare-with" value="{{$search_key}}" class="form-control"
                           placeholder="検索">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button">
                            <i class="las la-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            <div class="list-group list-group-light pt-2">
                <a href="#" class="list-group-item list-group-item-action px-3 border-0 active"
                   aria-current="true">地域</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">業界</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">職種</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">制度や特徴</a>
                <a href="#" class="list-group-item list-group-item-action px-3 border-0">使用状況</a>
            </div>
        </div>
        @if(app('request')->has('compare-with'))
            @if($companies_compare && $companies_compare->count() > 0)
                <div class="text-center mt-2 mb-3">
                    <h3>キーワードの検索結果 : {{$search_key}}</h3>
                </div>
                @foreach ($companies_compare as $company_compare_to)
                    <div class="card-deck mb-3 text-center">
                        <div class="card mb-4 box-shadow">
                            <div class="card-header">
                                <h5 class="my-0 font-weight-normal">
                                    {{$company_compare_to->name}} <a href="{{$company_compare_to->recruit_url}}">{{$company_compare_to->recruit_url}}</a>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="d-flex justify-content-around">
                                    <div class="w-50">
                                        <img class="w-100 border border-primary" src="{{$company_compare_to->image}}"
                                             alt="{{$company_compare_to->name}}">
                                    </div>
                                    <div>
                                        <h5 class="card-title pricing-card-title">
                                            有効掲載求人数
                                        </h5>
                                        <p>{{$company_compare_to->salary}}</p>
                                        <div class="mt-3 mb-4">
                                            <strong>Indeed</strong><span>（{{$company_compare_to->occupation}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <a href="{{route('company.detail',$company_compare_to->slug ?? "")}}"
                                       class="btn btn-lg btn-block btn-outline-primary w-25 mt-4 ml-auto mr-auto font-sm">
                                        <i class="las la-eye"></i> 詳細を見る
                                    </a>
                                    <a href="{{route('company.compare', $company->id)}}?compare-to={{$company_compare_to->id}}"
                                       class="btn btn-lg btn-block btn-outline-success w-25 mt-4 ml-auto mr-auto font-sm">
                                        <i class="las la-arrow-circle-right"></i> 比較して選ぶ
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="d-flex justify-content-center align-items-center">{{$companies_compare->links()}}</div>
            @else
                @if($search_key)
                    <div class="p-5 text-primary font-2xl text-center"><i class="las la-exclamation-circle"></i>
                        検索結果がありません。もう一度お試しください
                    </div>
                @endif
            @endif
        @else
            <div class="card-deck mb-3 text-center mt-4">
                <div class="card mb-4 box-shadow">
                    <div class="card-header">
                        <h5 class="my-0 font-weight-normal">
                            {{$company->name}} <a href="{{$company->recruit_url}}">{{$company->recruit_url}}</a>
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-around">
                            <div class="w-50">
                                <img class="w-100 border border-primary" src="{{$company->image}}"
                                     alt="{{$company->name}}">
                            </div>
                            <div>
                                <h5 class="card-title pricing-card-title">
                                    有効掲載求人数
                                </h5>
                                <p>{{$company->salary}}</p>
                                <div class="mt-3 mb-4">
                                    <strong>Indeed</strong><span>（{{$company->occupation}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="border-top border-default">
                            <h4 class="pt-2 pb-2">会社紹介</h4>
                            <p><i class="las la-phone text-primary font-weight-bold"></i> {{$company->telephone}}</p>
                            <p><i class="lar la-envelope text-primary font-weight-bold"></i> {{$company->email}}</p>
                            <p><strong>業界</strong> : {{$company->industry}}</p>
                            <p><strong>職業</strong> : {{$company->occupation}}</p>
                            <p><strong>勤務地</strong> : {{$company->work_location}}</p>
                            <p><strong>参考記事</strong> : {!! $company->reference_article_1 !!}</p>
                            <p><strong>参考記事</strong> : {!! $company->reference_article_2 !!}</p>
                            <p><strong>参考記事</strong> : {!! $company->reference_article_3 !!}</p>
                        </div>
                        <a href="{{route('company.detail',$company->slug ?? "")}}"
                           class="btn btn-lg btn-block btn-outline-primary w-25 mt-4 ml-auto mr-auto font-sm">
                            <i class="las la-eye"></i> 詳細を見る
                        </a>
                    </div>
                </div>
                <div class="card mb-4 box-shadow">
                    @if($company_compare)
                        <div class="card-header">
                            <h5 class="my-0 font-weight-normal">
                                {{$company_compare->name}} <a href="{{$company_compare->recruit_url}}">{{$company_compare->recruit_url}}</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="d-flex justify-content-around">
                                <div class="w-50">
                                    <img class="w-100 border border-primary" src="{{$company_compare->image}}"
                                         alt="{{$company_compare->name}}">
                                </div>
                                <div>
                                    <h5 class="card-title pricing-card-title">
                                        有効掲載求人数
                                    </h5>
                                    <p>{{$company_compare->salary}}</p>
                                    <div class="mt-3 mb-4">
                                        <strong>Indeed</strong><span>（{{$company_compare->occupation}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="border-top border-default">
                                <h4 class="pt-2 pb-2">会社紹介</h4>
                                <p><i class="las la-phone text-primary font-weight-bold"></i> {{$company_compare->telephone}}</p>
                                <p><i class="lar la-envelope text-primary font-weight-bold"></i> {{$company_compare->email}}</p>
                                <p><strong>業界</strong> : {{$company_compare->industry}}</p>
                                <p><strong>職業</strong> : {{$company_compare->occupation}}</p>
                                <p><strong>勤務地</strong> : {{$company_compare->work_location}}</p>
                                <p><strong>参考記事</strong> : {!! $company_compare->reference_article_1 !!}</p>
                                <p><strong>参考記事</strong> : {!! $company_compare->reference_article_2 !!}</p>
                                <p><strong>参考記事</strong> : {!! $company_compare->reference_article_3 !!}</p>
                            </div>
                            <a href="{{route('company.detail',$company_compare->slug ?? "")}}"
                               class="btn btn-lg btn-block btn-outline-primary w-25 mt-4 ml-auto mr-auto font-sm">
                                <i class="las la-eye"></i> 詳細を見る
                            </a>
                        </div>
                    @else
                        <div class="w-100 h-100 d-flex flex-column justify-content-center align-items-center">
                            <div class="font-xl text-primary font-weight-bold"><i class="las la-plus-circle"></i>
                                選んで比べてみてください
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </div>

@endsection

@push('javascript')
@endpush
