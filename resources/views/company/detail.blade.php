@php use App\Models\UserCompanyFavorite; @endphp
@extends('main')

@section('title', $company->name)

@push('css')
    <style>
        .banner-company {
            height: 236px;
            width: 100%;
            overflow: hidden;
        }

        .banner-company img {
            width: 100%;
        }

        .company-logo {
            flex: 0 0 200px;
            width: 200px;
            height: 200px;
            margin-right: 20px;
        }

        .image-company {
            width: 100%;
            height: auto;
        }

        .company-header {
            padding: 20px 20px 20px 20px;
            background-color: #fff;
        }

        .company-name {
            flex-grow: 1;
        }

        .button-favorite {
            width: 200px;
            flex: 0 0 200px;
        }

        .content-pri {
            margin-top: 30px;
        }

        .content-left {
            padding: 20px;
            border-radius: 3px;
            box-shadow: -1px 1px 6px rgb(0 0 0 / 5%);
            background-color: #fff;
        }

        .content-right {
            padding: 20px;
            border-radius: 3px;
            box-shadow: -1px 1px 6px rgb(0 0 0 / 5%);
            background-color: #fff;
        }

        .company-header-content {
            box-shadow: -1px 1px 6px rgb(0 0 0 / 5%);
            border-radius: 3px;
        }
    </style>
@endpush

@section('main')
    @php
        $user_has_favorite = false;
        //check xem company này có phải yêu thích không
        if(bo_user()){
        $user_has_favorite = UserCompanyFavorite::where('user_id', bo_user()->getKey())
        ->where('company_id', $company->id)
        ->exists();
        }
    @endphp
    <div class="container">
        <div class="company-header-content">
            <div class="banner-company w-100">
                <img src="{{asset('images/banner-company.png')}}" alt="">
            </div>
            <div class="d-flex company-header">
                <div class="company-logo border border-primary d-flex align-items-center">
                    <img class="image-company" src="{{$company->image}}" alt="{{$company->name}}">
                </div>
                <div class="company-name">
                    <h4>{{$company->name}}
                    </h4>
                    <a href="{{$company->recruit_url}}">{{$company->recruit_url}}</a>
                    <p>賃金 : {{$company->salary}} ({{$company->min_salary}} - {{$company->max_salary}}) </p>
                </div>
                <div>
                    @if(bo_user())
                        <a type="button"
                           onclick="favorite(this)"
                           @if($user_has_favorite)
                               {{--                           có thì hiện xoá yêu thích --}}
                               class="btn btn-lg btn-block btn-danger font-sm h-25 button-favorite">
                            <i class="las la-heart-broken"></i> お気に入りから削除
                            @else
                                {{--                          không có thì hiện thêm yêu thích --}}
                                class="btn btn-lg btn-block btn-outline-danger font-sm h-25 button-favorite">
                                <i class="lab la-gratipay"></i> お気に入りに追加
                            @endif
                        </a>
                    @else
                        <a href="{{route('login')}}" type="button"
                           class="btn btn-lg btn-block btn-outline-danger font-sm h-25 button-favorite">
                            <i class="lab la-gratipay"></i> 詳細を見る
                        </a>
                    @endif
                    <a href="{{route('company.compare', $company->id)}}" class="btn btn-block btn-outline-success button-favorite"><i class="las la-exchange-alt"></i> 比較</a>
                </div>
            </div>
        </div>
        <div class="content-pri">
            <div class="row">
                <div class="col-md-9">
                    <div class="content-left">
                        <h4 class="pt-2 pb-2">会社紹介</h4>
                        <p><i class="las la-phone text-primary font-weight-bold"></i> {{$company->telephone}}</p>
                        <p><i class="lar la-envelope text-primary font-weight-bold"></i> {{$company->email}}</p>
                        <div class="border-top border-default pb-4"></div>
                        <p><strong>業界</strong> : {{$company->industry}}</p>
                        <p><strong>職業</strong> : {{$company->occupation}}</p>
                        <p><strong>勤務地</strong> : {{$company->work_location}}</p>
                        <div class="border-top border-default pb-4"></div>
                        <p><strong>参考記事</strong> : {!! $company->reference_article_1 !!}</p>
                        <p><strong>参考記事</strong> : {!! $company->reference_article_2 !!}</p>
                        <p><strong>参考記事</strong> : {!! $company->reference_article_3 !!}</p>
                    </div>
                    <div class="content-left mt-4">
                        <h4 class="pt-2 pb-2">会社からの宣伝
                        </h4>

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="content-right">
                        <h4 class="pt-2 pb-2">会社住所</h4>
                        <div>
                            <i class="las la-map-marker text-primary"></i>
                            <span class="text">
                                {{$company->address}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@if(bo_user())
    @push('javascript')
        <script>
            function favorite(that) {
                $.post("{{route('favorite.change', $company->id)}}",
                    {
                        "_token": "{{csrf_token()}}"
                    }
                    , function (result) {
                        result = JSON.parse(result);
                        if (result.action === 'add') {
                            //vừa thêm mới thì xóa
                            $(that).removeClass('btn-outline-danger');
                            $(that).addClass('btn-danger');
                            $(that).html(`<i class="las la-heart-broken"></i> お気に入りから削除`);
                        } else if (result.action === 'remove') {
                            //vừa xóa xong thì thêm mới
                            $(that).removeClass('btn-danger');
                            $(that).addClass('btn-outline-danger');
                            $(that).html(`<i class="lab la-gratipay"></i> お気に入りに追加`);
                        }
                    })
            }
        </script>
    @endpush
@endif
