@extends('main')

@section('title', __('Search'))

@section('main')

    <div class="wrapper">
        <!-- Sidebar  -->
        @include('dashboard.left-menu')

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="las la-list"></i>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
                                <span class="nav-link" id="active-left-menu-dashboard"></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div>
                @yield('main-content')
            </div>

        </div>
    </div>

@endsection

@push('javascript')
    <script>
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            $('#active-left-menu-dashboard').text($('#sidebar ul li a.active').text());
        });
    </script>
@endpush
