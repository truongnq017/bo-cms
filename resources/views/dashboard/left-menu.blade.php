<style>
    p {
        color: #999;
    }

    .link-dashboard,
    .link-dashboard:hover,
    .link-dashboard:focus {
        color: inherit;
        text-decoration: none;
        transition: all 0.3s;
    }

    .navbar {
        padding: 15px 10px;
        background: #fff;
        border: none;
        border-radius: 0;
        margin-bottom: 40px;
        box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
    }

    .navbar-btn {
        box-shadow: none;
        outline: none !important;
        border: none;
    }

    .line {
        width: 100%;
        height: 1px;
        border-bottom: 1px dashed #ddd;
        margin: 40px 0;
    }

    .wrapper {
        display: flex;
        width: 100%;
        align-items: stretch;
    }

    #sidebar {
        min-width: 250px;
        max-width: 250px;
        background: #7386D5;
        color: #fff;
        transition: all 0.3s;
    }

    #sidebar.active {
        margin-left: -250px;
    }

    #sidebar .sidebar-header {
        padding: 20px;
        background: #6d7fcc;
    }

    #sidebar ul.components {
        padding: 20px 0;
        border-bottom: 1px solid #47748b;
    }

    #sidebar ul p {
        color: #fff;
        padding: 10px;
    }

    #sidebar ul li a {
        padding: 10px;
        display: block;
    }

    #sidebar ul li a.link-dashboard:hover {
        color: #7386D5;
        background: #fff;
    }

    #sidebar ul li a.link-dashboard.active{
        color: #7386D5;
        background: #f5f2f2;
    }

    #sidebar ul li.active > .link-dashboard,
    .link-dashboard[aria-expanded="true"] {
        color: #fff;
        background: #6d7fcc;
    }

    .link-dashboard[data-toggle="collapse"] {
        position: relative;
    }

    .dropdown-toggle::after {
        display: block;
        position: absolute;
        top: 50%;
        right: 20px;
        transform: translateY(-50%);
    }

    ul ul .link-dashboard {
        font-size: 0.9em !important;
        padding-left: 30px !important;
        background: #6d7fcc;
    }

    ul.CTAs {
        padding: 20px;
    }

    ul.CTAs .link-dashboard {
        text-align: center;
        font-size: 0.9em !important;
        display: block;
        border-radius: 5px;
        margin-bottom: 5px;
    }

    .link-dashboard .download {
        background: #fff;
        color: #7386D5;
    }

    .link-dashboard.article,
    .link-dashboard.article:hover {
        background: #6d7fcc !important;
        color: #fff !important;
    }

    .title-left-menu{
        padding: 16px 16px 16px 16px;
        color: #FFF;
        font-weight: bold;
        border-bottom: 1px solid #47748b;
        text-align: center;
    }

    #content {
        width: 100%;
        padding: 20px;
        min-height: 100vh;
        transition: all 0.3s;
    }

    @media (max-width: 768px) {
        #sidebar {
            margin-left: -250px;
        }

        #sidebar.active {
            margin-left: 0;
        }

        #sidebarCollapse span {
            display: none;
        }
    }
</style>

<nav id="sidebar">
    <div class="sidebar-header">
        <h5>{{bo_user()->name}}</h5>
    </div>
    <p class="title-left-menu">マイページ</p>
    <ul class="list-unstyled components">
        <li>
            <a href="{{route('user.dashboard')}}" @class([
                'active' => Route::is('user.dashboard'),
                'link-dashboard' => true,
            ])>個人情報</a>
        </li>
        <li>
            <a href="{{route('user.favorite')}}"@class([
                'active' => Route::is('user.favorite'),
                'link-dashboard' => true,
            ])>お気に入り登録した企業</a>
        </li>
        <li>
            <a class="link-dashboard" href="#">企業からのメール</a>
        </li>
        <li>
            <a class="link-dashboard" href="#">スケジュール</a>
        </li>
        <li>
            <a class="link-dashboard" href="#">応募済みの企業</a>
        </li>
    </ul>

    <ul class="list-unstyled CTAs">
        <li>
            <a class="link-dashboard border-light" href="{{ route('logout') }}" class="article"><i class="las la-sign-out-alt"></i> {{ trans('bo::base.logout') }}</a>
        </li>
    </ul>
</nav>
