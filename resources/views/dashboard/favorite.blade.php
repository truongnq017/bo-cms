@extends('dashboard.main')

@section('title', __('Dashboard User Favorite'))

@push('css')
    <style>
        ul.pagination li:last-child a.page-link {
            background-color: #fff !important;
        }

        ul.pagination li:first-child a.page-link {
            background-color: #fff !important;
        }
    </style>
@endpush

@section('main-content')
    @if($user_favorites)
        <div class="container mt-4">
            <div class="text-center mt-2 mb-3">
                <h3>会社一覧</h3>
            </div>
            @foreach ($user_favorites as $user_favorite)
                <div class="card-deck mb-3 text-center">
                    <div class="card mb-4 box-shadow">
                        <div class="card-header">
                            <h5 class="my-0 font-weight-normal">
                                {{$user_favorite->companies->name}} <a href="{{$user_favorite->companies->recruit_url}}">{{$user_favorite->companies->recruit_url}}</a>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="d-flex justify-content-around">
                                <div class="w-50">
                                    <img class="w-100 border border-primary" src="{{$user_favorite->companies->image}}" alt="{{$user_favorite->companies->name}}">
                                </div>
                                <div>
                                    <h5 class="card-title pricing-card-title">
                                        有効掲載求人数
                                    </h5>
                                    <p>{{$user_favorite->companies->salary}}</p>
                                    <div class="mt-3 mb-4">
                                        <strong>Indeed</strong><span>（{{$user_favorite->companies->occupation}}</span>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('company.detail',$user_favorite->companies->slug ?? "")}}"
                                    class="btn btn-lg btn-block btn-outline-primary w-25 mt-4 ml-auto mr-auto font-sm">
                                <i class="las la-eye"></i> 詳細を見る
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="d-flex justify-content-center align-items-center">{{$user_favorites->links()}}</div>
        </div>
    @else
        <p class="text-primary text-xl-center font-weight-bold">お気に入りに会社を追加していません</p>
    @endif

@endsection

