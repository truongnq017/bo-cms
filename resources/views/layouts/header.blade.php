<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mysto - @yield('title')</title>
    @if (config('bo.base.styles') && count(config('bo.base.styles')))
        @foreach (config('bo.base.styles') as $path)
            <link rel="stylesheet" type="text/css" href="{{ asset($path).'?v='.config('bo.base.cachebusting_string') }}">
        @endforeach
    @endif
    <link rel="stylesheet" href="{{asset('assets/css/app.css')}}">

    @if (config('bo.base.mix_styles') && count(config('bo.base.mix_styles')))
        @foreach (config('bo.base.mix_styles') as $path => $manifest)
            <link rel="stylesheet" type="text/css" href="{{ mix($path, $manifest) }}">
        @endforeach
    @endif
    @stack('css')
</head>

