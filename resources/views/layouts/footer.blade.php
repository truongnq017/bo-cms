{{--<hr class="mt-0 mb-0">--}}
{{--<div class="container">--}}
{{--    <footer class="pt-4 my-md-5 pt-md-5">--}}
{{--        <div class="row">--}}
{{--            <div class="col-12 col-md">--}}
{{--                <img class="mb-2" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">--}}
{{--                <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md">--}}
{{--                <h5>Features</h5>--}}
{{--                <ul class="list-unstyled text-small">--}}
{{--                    <li><a class="text-muted" href="#">Cool stuff</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Random feature</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Team feature</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Stuff for developers</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Another one</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Last time</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md">--}}
{{--                <h5>Resources</h5>--}}
{{--                <ul class="list-unstyled text-small">--}}
{{--                    <li><a class="text-muted" href="#">Resource</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Resource name</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Another resource</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Final resource</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md">--}}
{{--                <h5>About</h5>--}}
{{--                <ul class="list-unstyled text-small">--}}
{{--                    <li><a class="text-muted" href="#">Team</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Locations</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Privacy</a></li>--}}
{{--                    <li><a class="text-muted" href="#">Terms</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </footer>--}}
{{--</div>--}}

</body>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

@if (config('bo.base.scripts') && count(config('bo.base.scripts')))
    @foreach (config('bo.base.scripts') as $path)
        <script type="text/javascript" src="{{ asset($path).'?v='.config('bo.base.cachebusting_string') }}"></script>
    @endforeach
@endif

@if (config('bo.base.mix_scripts') && count(config('bo.base.mix_scripts')))
    @foreach (config('bo.base.mix_scripts') as $path => $manifest)
        <script type="text/javascript" src="{{ mix($path, $manifest) }}"></script>
    @endforeach
@endif
</html>
