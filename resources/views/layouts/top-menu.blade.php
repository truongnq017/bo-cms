<div class="bg-white border-bottom border-info text-info">
    <div class="container d-flex flex-column flex-md-row align-items-center p-1 px-md-4 box-shadow">
        <h5 class="my-0 mr-md-auto"><a href="{{route('home')}}" class="text-info font-2xl text-decoration-none"
                                       style="font-weight: 600 !important;">MYSTO</a></h5>
        @if (bo_auth()->guest())
            <a href="{{route('login')}}" class="text-decoration-none">
                <span role="img" aria-label="export" class="text-info d-block text-center"
                      style="font-size: 28px;"><svg viewBox="64 64 896 896" focusable="false"
                                                    data-icon="export" width="1em" height="1em"
                                                    fill="currentColor" aria-hidden="true"><path
                            d="M888.3 757.4h-53.8c-4.2 0-7.7 3.5-7.7 7.7v61.8H197.1V197.1h629.8v61.8c0 4.2 3.5 7.7 7.7 7.7h53.8c4.2 0 7.7-3.4 7.7-7.7V158.7c0-17-13.7-30.7-30.7-30.7H158.7c-17 0-30.7 13.7-30.7 30.7v706.6c0 17 13.7 30.7 30.7 30.7h706.6c17 0 30.7-13.7 30.7-30.7V765.1c0-4.3-3.5-7.7-7.7-7.7zm18.6-251.7L765 393.7c-5.3-4.2-13-.4-13 6.3v76H438c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h314v76c0 6.7 7.8 10.5 13 6.3l141.9-112a8 8 0 000-12.6z"></path></svg>
                </span>
                <span style="font-size: 12px;" class="text-info d-block text-center">ログイン</span>
            </a>
        @else
            <div class="d-flex">
                <a href="{{ route('logout') }}" class="text-decoration-none mr-4">
                    <span role="img" aria-label="export"
                          class="text-info d-block text-center" style="font-size: 28px;"><svg
                            viewBox="64 64 896 896" focusable="false" data-icon="export" width="1em" height="1em"
                            fill="currentColor" aria-hidden="true"><path
                                d="M888.3 757.4h-53.8c-4.2 0-7.7 3.5-7.7 7.7v61.8H197.1V197.1h629.8v61.8c0 4.2 3.5 7.7 7.7 7.7h53.8c4.2 0 7.7-3.4 7.7-7.7V158.7c0-17-13.7-30.7-30.7-30.7H158.7c-17 0-30.7 13.7-30.7 30.7v706.6c0 17 13.7 30.7 30.7 30.7h706.6c17 0 30.7-13.7 30.7-30.7V765.1c0-4.3-3.5-7.7-7.7-7.7zm18.6-251.7L765 393.7c-5.3-4.2-13-.4-13 6.3v76H438c-4.4 0-8 3.6-8 8v56c0 4.4 3.6 8 8 8h314v76c0 6.7 7.8 10.5 13 6.3l141.9-112a8 8 0 000-12.6z"></path></svg></span>
                    <span style="font-size: 12px;" class="text-info d-block text-center">ログアウト</span></a>
                <a href="{{route('user.favorite')}}" class="text-decoration-none">
                    <span role="img" aria-label="crown" class="text-info d-block text-center"
                          style="font-size: 28px;"><svg viewBox="64 64 896 896" focusable="false"
                                                        data-icon="crown" width="1em" height="1em"
                                                        fill="currentColor" aria-hidden="true"><path
                                d="M899.6 276.5L705 396.4 518.4 147.5a8.06 8.06 0 00-12.9 0L319 396.4 124.3 276.5c-5.7-3.5-13.1 1.2-12.2 7.9L188.5 865c1.1 7.9 7.9 14 16 14h615.1c8 0 14.9-6 15.9-14l76.4-580.6c.8-6.7-6.5-11.4-12.3-7.9zm-126 534.1H250.3l-53.8-409.4 139.8 86.1L512 252.9l175.7 234.4 139.8-86.1-53.9 409.4zM512 509c-62.1 0-112.6 50.5-112.6 112.6S449.9 734.2 512 734.2s112.6-50.5 112.6-112.6S574.1 509 512 509zm0 160.9c-26.6 0-48.2-21.6-48.2-48.3 0-26.6 21.6-48.3 48.2-48.3s48.2 21.6 48.2 48.3c0 26.6-21.6 48.3-48.2 48.3z"></path></svg></span>
                    <span style="font-size: 12px;" class="text-info d-block text-center">お気に入りリスト</span></a>
                <a href="{{route('user.dashboard')}}" class="text-decoration-none ml-4">
                    <span role="img" aria-label="user" class="text-info d-block text-center"
                          style="font-size: 28px;"><svg viewBox="64 64 896 896" focusable="false"
                                                        data-icon="user" width="1em" height="1em"
                                                        fill="currentColor" aria-hidden="true"><path
                                d="M858.5 763.6a374 374 0 00-80.6-119.5 375.63 375.63 0 00-119.5-80.6c-.4-.2-.8-.3-1.2-.5C719.5 518 760 444.7 760 362c0-137-111-248-248-248S264 225 264 362c0 82.7 40.5 156 102.8 201.1-.4.2-.8.3-1.2.5-44.8 18.9-85 46-119.5 80.6a375.63 375.63 0 00-80.6 119.5A371.7 371.7 0 00136 901.8a8 8 0 008 8.2h60c4.4 0 7.9-3.5 8-7.8 2-77.2 33-149.5 87.8-204.3 56.7-56.7 132-87.9 212.2-87.9s155.5 31.2 212.2 87.9C779 752.7 810 825 812 902.2c.1 4.4 3.6 7.8 8 7.8h60a8 8 0 008-8.2c-1-47.8-10.9-94.3-29.5-138.2zM512 534c-45.9 0-89.1-17.9-121.6-50.4S340 407.9 340 362c0-45.9 17.9-89.1 50.4-121.6S466.1 190 512 190s89.1 17.9 121.6 50.4S684 316.1 684 362c0 45.9-17.9 89.1-50.4 121.6S557.9 534 512 534z"></path></svg></span>
                    <span style="font-size: 12px;" class="text-info d-block text-center">マイページ</span></a>
            </div>
        @endif
    </div>
</div>
