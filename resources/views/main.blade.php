<!DOCTYPE html>
<html>

@include('layouts.header')

<body class="bg-white">
@include('layouts.top-menu')

{{-- Main --}}
@yield('main')

{{-- Footer --}}
@include('layouts.footer')

</body>

@stack('javascript')

</html>
