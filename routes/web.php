<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PublicController::class, 'index'])->name('home');

Route::get('home', function () {
    return redirect(route('home'));
});

Route::get('login', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login.post');

Route::get('register', [\App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register.index');
Route::post('register', [\App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register.post');

Route::get('logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::get('search', [\App\Http\Controllers\SearchController::class, 'search'])->name('search');

Route::group(['middleware' => ['auth']], function () {
    Route::get('dashboard/user', [\App\Http\Controllers\DashBoardController::class, 'user'])->name('user.dashboard');
    Route::get('dashboard/favorite', [\App\Http\Controllers\DashBoardController::class, 'userFavorite'])->name('user.favorite');

    Route::post('edit-account-info', [\App\Http\MyAccountController::class, 'postAccountInfoForm'])->name('account.info.store');
    Route::post('change-password', [\App\Http\MyAccountController::class, 'postChangePasswordForm'])->name('account.password');

    Route::post('favorite/change/{company_id}', [\App\Http\Controllers\DashBoardController::class, 'changeFavorite'])->name('favorite.change');
});

Route::get('/companies/{company}', [\App\Http\Controllers\CompanyController::class, 'detail'])->name('company.detail');
Route::get('/compare-company/{company_id}', [\App\Http\Controllers\CompanyController::class, 'compareCompany'])->name('company.compare');
