### Build CMS base on Laravel...

* `composer install`
* `cp .env.example .env`
* `php artisan migrate`
* `php artisan bo:cms:install`
* `php artisan generate:slug`
