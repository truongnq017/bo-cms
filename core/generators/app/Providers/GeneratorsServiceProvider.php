<?php

namespace Bo\Generators\Providers;

use Bo\Generators\Console\Commands\BuildBackpackCommand;
use Bo\Generators\Console\Commands\ChartBackpackCommand;
use Bo\Generators\Console\Commands\ChartControllerBackpackCommand;
use Bo\Generators\Console\Commands\ConfigBackpackCommand;
use Bo\Generators\Console\Commands\CrudBackpackCommand;
use Bo\Generators\Console\Commands\CrudControllerBackpackCommand;
use Bo\Generators\Console\Commands\CrudModelBackpackCommand;
use Bo\Generators\Console\Commands\CrudOperationBackpackCommand;
use Bo\Generators\Console\Commands\CrudRequestBackpackCommand;
use Bo\Generators\Console\Commands\ModelBackpackCommand;
use Bo\Generators\Console\Commands\PageBackpackCommand;
use Bo\Generators\Console\Commands\PageControllerBackpackCommand;
use Bo\Generators\Console\Commands\RequestBackpackCommand;
use Bo\Generators\Console\Commands\ViewBackpackCommand;
use Illuminate\Support\ServiceProvider;

class GeneratorsServiceProvider extends ServiceProvider
{
    protected array $commands = [
        BuildBackpackCommand::class,
        ConfigBackpackCommand::class,
        CrudModelBackpackCommand::class,
        CrudControllerBackpackCommand::class,
        ChartControllerBackpackCommand::class,
        CrudOperationBackpackCommand::class,
        CrudRequestBackpackCommand::class,
        CrudBackpackCommand::class,
        ChartBackpackCommand::class,
        ModelBackpackCommand::class,
        PageBackpackCommand::class,
        PageControllerBackpackCommand::class,
        RequestBackpackCommand::class,
        ViewBackpackCommand::class,
    ];

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);
    }
}
