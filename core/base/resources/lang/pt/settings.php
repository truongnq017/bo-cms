<?php

return [
    /*
  |--------------------------------------------------------------------------
  | Settings Language Lines
  |--------------------------------------------------------------------------
  |
  |
  */
    'name'             => 'Nome',
    'value'            => 'Valor',
    'description'      => 'Descrição',
    'setting_singular' => 'configuração',
    'setting_plural'   => 'configurações',
    'type'             => 'Modelo'
];
