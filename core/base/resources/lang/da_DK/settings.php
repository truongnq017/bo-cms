<?php

return [
    /*
  |--------------------------------------------------------------------------
  | Settings Language Lines
  |--------------------------------------------------------------------------
  |
  */
    'name'             => 'Navn',
    'value'            => 'værdi',
    'description'      => 'beskrivelse',
    'setting_singular' => 'indstilling',
    'setting_plural'   => 'indstillinger',
    'type'             => 'Type',
];
