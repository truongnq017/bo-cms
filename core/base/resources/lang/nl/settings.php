<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Naam',
    'value'            => 'Waarde',
    'description'      => 'Beschrijving',
    'setting_singular' => 'instelling',
    'setting_plural'   => 'instellingen',
    'type'             => 'Type'
];
