<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Nome',
    'value'            => 'Valore',
    'description'      => 'Descrizione',
    'setting_singular' => 'impostazione',
    'setting_plural'   => 'impostazioni',
    'type'             => 'genere',

];
