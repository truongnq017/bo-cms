<?php

return [
    /*
  |--------------------------------------------------------------------------
  | Settings Language Lines
  |--------------------------------------------------------------------------
  |
  */
    'name'             => '名称',
    'value'            => '值',
    'description'      => '描述',
    'setting_singular' => '设置',
    'setting_plural'   => '设置',
    'type'             => '类型',

];
