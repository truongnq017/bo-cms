<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Nombre',
    'value'            => 'Valor',
    'description'      => 'Descripción',
    'setting_singular' => 'configuración',
    'setting_plural'   => 'configuraciones',
    'type'             => 'Escribe',

];
