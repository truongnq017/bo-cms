<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Tên',
    'value'            => 'Giá trị',
    'description'      => 'Mô tả',
    'type'             => 'Kiểu',
    'setting_singular' => 'cấu hình',
    'setting_plural'   => 'các cấu hình',

];
