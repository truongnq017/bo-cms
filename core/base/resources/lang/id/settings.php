<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Nama',
    'value'            => 'Nilai',
    'description'      => 'Deskripsi',
    'setting_singular' => 'Pengaturan',
    'setting_plural'   => 'Pengaturan',
    'type'             => 'Tipe',
];
