<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Naziv',
    'value'            => 'Vrednost',
    'description'      => 'Opis',
    'setting_singular' => 'Podešavanje',
    'setting_plural'   => 'Podešavanja',
    'type'             => 'Type'
];
