<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Name',
    'value'            => 'Wert',
    'description'      => 'Beschreibung',
    'setting_singular' => 'Einstellung',
    'setting_plural'   => 'Einstellungen',
    'type'             => 'Type',

];
