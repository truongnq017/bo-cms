<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Adı',
    'value'            => 'Değer',
    'description'      => 'Açıklama',
    'setting_singular' => 'ayar',
    'setting_plural'   => 'ayarlar',
    'type'             => 'Type',
];
