<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Name',
    'value'            => 'Value',
    'description'      => 'Description',
    'setting_singular' => 'setting',
    'setting_plural'   => 'settings',
    'type'             => 'Type',
];
