<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Settings Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'name'             => 'Nom',
    'value'            => 'Valeur',
    'description'      => 'Description',
    'setting_singular' => 'paramètre',
    'setting_plural'   => 'paramètres',
    'type'             => 'Taper',

];
